# Afspraakbot

## Requirements

- Docker
- Git
- Visual Studio Code (or a different code editor)

### 1. Git clone the repository
Open a terminal and git clone this repo
```shell
git clone https://gitlab.com/virtuele-gemeente-assistent/afspraakbot.git
```
### 1. Go to project dir

```shell
cd afspraakbot
```

### 2. Pull images

```shell
docker-compose pull
```
wait...

### 3. Run project

##### 3.1 Train model

```shell
docker-compose run afspraakbot train -d domain
```
wait...

#### 3.2 run afspraakbot shell
Maakt het mogelijk om te chatten met je assistent via de command line
```shell
docker-compose run afspraakbot shell
```

### 4. Run in production

##### 4.1 Run with logs in console

```shell
docker-compose up
```

##### 4.2 Run as process

```shell
docker-compose up -d
```

### 5. Other Docker commands

##### 5.1 Stop containers
```shell
docker-compose down
```

##### 5.2 Restart containers
```shell
docker-compose restart
```


##### 5.3 Remove image
```shell
docker rmi [image-name]
```
or
```shell
docker rmi -f [image-name]
```

##### 5.4 Show Docker process
```shell
docker ps
docker-compose ps
```

##### 5.5 Show Docker images
```shell
docker images
docker-compose images
```

# Command Line Cheat Sheet

#### rasa shell
Maakt het mogelijk om te chatten met je assistent via de command line
```shell
docker-compose run afspraakbot shell
```

#### rasa train
Traint het model en slaat het getrainde model op in ./models folder
```shell
docker-compose run afspraakbot train -d domain
```

#### rasa data validate
Controleert het domein en core op inconsistenties
```shell
docker-compose run afspraakbot data validate -d domain
```