import os
import requests
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted, ConversationPaused
from rasa_sdk import Tracker
import logging
# from .allmanak_api import allmanak_api_url
from .utils import getsuggestionbuttonforintent, transform_entities, transform_suggestions, process_event_text
from .constants import DEFAULT_REQUEST_TIMEOUT


logger = logging.getLogger(__name__)


def get_chat_history(trackerlog, entities, suggestions):
    chat_history = []
    previous_id = None
    for event in trackerlog["events"]:
        if not event.get("text"):
            continue

        if event['event'] == 'user':
            if not event.get("message_id"):
                continue

            if event['text'].startswith("/greet"):
                continue

            # Workaround for duplicate user events after clicking button
            # from ask_affirmation action, see: https://forum.rasa.com/t/user-messages-duplication/23746/29
            if event.get("message_id") == previous_id:
                logger.info(f"Duplicate user event encountered, skipping {event}")
                continue
            previous_id = event.get("message_id")

            processed_text = process_event_text(event, entities, suggestions)

            chat_history.append({"event": event["event"], "text": processed_text})
        elif event['event'] == 'bot':
            processed_text = process_event_text(event, entities, suggestions)

            chat_history.append({"event": event["event"], "text": processed_text})
    return chat_history


def convert_chat_history_to_plaintext(chat_history):
    plaintext = ""
    for message in chat_history:
        if message["event"] == "user":
            plaintext += f"Gebruiker: {message['text']}\n\n"
        elif message["event"] == "bot":
            plaintext += f"Gem: {message['text']}\n\n"
    return plaintext


class BaseActionEscalatie:
    availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}"

    def escalate_to_obi(self, dispatcher, tracker, domain):
        livechat = False

        # user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})

        user_event_metadata = None

        events = tracker.events[::-1]
        for x in events:
            if x["event"] == "user" and x.get("metadata", None):
                if x["metadata"].get("livechat", None) != None:
                    user_event_metadata = x["metadata"]
                    break

        livechat_available = False
        guid = user_event_metadata.get("guid")
        if guid:
            try:
                availability_resp = requests.get(
                    self.availability_uri.format(guid), headers={"accept": "application/json"},
                    timeout=DEFAULT_REQUEST_TIMEOUT
                ).json()
            except requests.exceptions.RequestException as e:
                dispatcher.utter_message(template="utter_la_no_human_available")
                logger.error(f"Error occurred while requesting availability API: {e}")
            else:
                availability = availability_resp['available']
                if availability:
                    livechat_available = True
                    logger.info("OBI4wan: Agent available") # DEBUG
                else:
                    logger.info("OBI4wan: Agent NOT available") # DEBUG

        municipality = user_event_metadata.get("municipality")
        if livechat_available and municipality:
            livechat = True

        if livechat:
            entities = transform_entities(tracker)
            suggestions = transform_suggestions(domain)

            trackerlog = tracker.current_state()
            chat_history = get_chat_history(trackerlog, entities, suggestions)

            if self.name() == "action_escalatie":
                dispatcher.utter_message(template="utter_la_livechatopen_doorschakeling2")

            logger.info(str(tracker.latest_message))

            # Send the municipality to determine the avatar used for livechat
            dispatcher.utter_message(
                json_message={'municipality': municipality}
            )
            return[ConversationPaused()]
        else:
            municipality_status = tracker.get_slot("feature_content") #checkt of de betreffende gemeente is aangesloten
            gemeente_telefoonnummer = tracker.get_slot("gemeente_telefoonnummer")
            # municipality = municipality

            if municipality == None:
                user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})
                municipality = user_event_metadata.get("municipality")

            # if municipality_status == "niet_aangesloten":
            #     allmanakinfo = allmanak_api_url(municipality)
            #     logger.info(allmanakinfo)

            #     #hieronder wordt er eerst geprobeerd om een email-adres op te halen uit de Allmanak API. Indien die niet beschikbaar is (except) dan wordt er een utter verstuurd met uitsluitend een telefoonnummer (die is altijd beschikbaar).
            #     try:
            #         gemeente_emailadres = allmanakinfo[0]["contact"]["emailadres"]["value"]
            #         dispatcher.utter_template('utter_la_gemeente_telefoonnummer_email_landelijk', tracker, municipality="{0}".format(municipality), gemeente_emailadres="{0}".format(gemeente_emailadres), gemeente_telefoonnummer = "{0}".format(gemeente_telefoonnummer))
            #         return []

            #     except:
            #         dispatcher.utter_template('utter_la_gemeente_telefoonnummer_landelijk', tracker, municipality="{0}".format(municipality), gemeente_telefoonnummer = "{0}".format(gemeente_telefoonnummer))
            #         return []


            else:
                # allmanakinfo = allmanak_api_url(municipality)
                # logger.info(allmanakinfo)
                dispatcher.utter_template('utter_lo_livechatgesloten', tracker)

                # Aangesloten gemeenten kunnen de utter_la_gemeente_contact naar wens aanpassen.
                dispatcher.utter_template('utter_lo_gemeente_contact_lokaal_livechat', tracker)
                return []

    def run(self, dispatcher, tracker, domain):
        # user_event_metadata = tracker.get_last_event_for("user").get("metadata", {})
        user_event_metadata = {}

        events = tracker.events[::-1]
        for x in events:
            if x["event"] == "user" and x.get("metadata", None):
                if x["metadata"].get("municipality", None) != None:
                    user_event_metadata = x["metadata"]
                    break

        municipality = user_event_metadata.get("municipality")

        if user_event_metadata.get("livechat") == "obi4wan":
            return self.escalate_to_obi(dispatcher, tracker, domain)
        else:
            entities = transform_entities(tracker)
            suggestions = transform_suggestions(domain)
            trackerlog = tracker.current_state()
            chat_history = get_chat_history(trackerlog, entities, suggestions)
            chat_history_plain = convert_chat_history_to_plaintext(chat_history)

            dispatcher.utter_message(
                json_message={
                    "escalate": "livechat",
                    "payload": {
                        "livechat": "start",
                        # "chat_history": chat_history,
                        # "chat_history_plain": chat_history_plain,
                    },
                    "municipality": municipality
                }
            )
            return [ConversationPaused()]


class ActionEscalatie(BaseActionEscalatie, Action):
    def name(self):
        return "action_escalatie"
