from typing import Dict, Text, Any, List, Optional, Union

from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, EventType, ActiveLoop, AllSlotsReset
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction, FormAction, REQUESTED_SLOT
import logging
from zeep import Client, Settings, helpers
from datetime import datetime, timedelta
from fuzzywuzzy import fuzz
import os
import json

from .appointments.client import *
from .appointments.jcc import JCCClient

logger = logging.getLogger(__name__)

token = os.getenv('API_TOKEN')
client = JCCClient(token=token)

#TODO: Make it more dynamic with JCCClient API
#TODO: place in separate object and file
PRODMAPPING = {"paspoort": {"productdesc": "paspoort aanvragen", "productid": "19"},
               "id-kaart": {"productdesc": "id-kaart aanvragen", "productid": "19"},
               "rijbewijs":{"productdesc": "rijbewijs aanvragen","productid": "23"},
              }

#TODO: Make it more dynamic with JCCClient API
#TODO: place in separate object and file
CODEMAPPING = {-1: {"description": "Onbekende fout"},
               0: {"description": "Boeking succesvol"},
               1: {"description": "Begindatumtijd afspraak ligt in verleden"},
               3: {"description": "Locatie niet gevonden"},
               4: {"description": "Geen klantnaam meegegeven"},
               5: {"description": "Geen plannerobject beschikbaar"},
               6: {"description": "Plannerobject heeft overlap"},
               7: {"description": "Productnummer(s) niet geldig"},
               12: {"description": "Niet alle verplichte klantvelden zijn gevuld"},
               13: {"description": "Kon met digid ingelogde klant niet vinden of verifiëren"},
               14: {"description": "Kon geen veld vinden met de meegegeven naam"}
                }

#TODO: Can be removed as we will use JCCClient
#TODO: remove directory, path_to_wdsl, settings
#to get the current working directory
directory = os.getcwd()
path_to_wdsl = "/app/actions/WSDL_afspraak_maken.wsdl"
settings = Settings(extra_http_headers={'Authorization': 'Basic ' + token})

class ValidateForm(FormAction):
    """Example of a form validation action."""

    def name(self) -> Text:
        return "form_afspraak"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        required_slots = ["product", "locatie", "afspraak_datetime", "achternaam", "geboortedatum", "email"]
        logger.info("required_slots: {}".format(required_slots))
        return required_slots

    def get_location_names(self, product_id, product_description):
        
        """
        params:
        product_id = productnummer
        product_description = Productomschrijving, zelf definieerbaar in JCC-Afspraken

        returns:
        location_dict = dictionary containing locations and synonyms for location
        location_ids = dictionary containing locations and location ids
        """
        # get location names
        location_name_list = client.get_locations([AppointmentProduct(identifier=product_id, 
                                                                      name=product_description)])
        # init dicts
        location_dict = {}
        location_ids = {}
        #TODO: transform to comprehension list
        for idx, _ in enumerate(location_name_list):
            # get values
            location_id = location_name_list[idx].identifier
            location_name = location_name_list[idx].name
            location_ids[location_name] = location_id

            # location_name_list 
            location_split = location_name.split()

            # add to dictionary
            location_dict[location_name] = location_name

            # check if split contains more values
            if len(location_split) >= 1:
                # loop in synonyms
                for synonym in location_split[1:]:
                    # append synonyms
                    location_dict[synonym] = location_name
                    
        return location_dict, location_ids

    def fuzzy_check(self, location, user_message):
        Ratio = fuzz.partial_ratio(location, user_message)
        if Ratio >= 80:
            return True
        else:
            return False

    def detect_location(self, user_message,
                        product_id, product_description):
        
        #TODO needs to be a list product_id, product_description
        location_dict, location_ids = self.get_location_names(product_id=product_id, 
                                                              product_description=product_description)
        
        logger.info("LOC DICT {}".format(location_dict))
        # init dict
        result = {}
        # get all stadswinkel
        stadswinkel_list = list(location_dict.keys())
        # init for detected_location
        detected_location = None
        # loop in stadswinkel 
        for winkel in stadswinkel_list:
            # Check whether there is an exact match
            if winkel.lower() in user_message.lower():
                logger.info("EXACT MATCH {}".format(winkel))
                # pass location
                detected_location = location_dict[winkel]
                break
            else:
                # if no match, check for a fuzzy match
                boolean = self.fuzzy_check(winkel, user_message)
                # check if fuzzy_check is True
                if boolean:
                    detected_location = location_dict[winkel]
        
        if not detected_location:
            result.update(location = None, locationID = None)
        
        else:
            result.update(location = detected_location, locationID = location_ids[detected_location])
        
        return result

    def create_appointment_details(self, location_id: str, product_id: str, 
                               last_name: str, start_date, end_date,
                               birth_date, client_verified, client_recurring,
                               client_mail: str):

        """
        params: 
        location_id = Unieke nummer van een locatie.
        product_id = Unieke nummer van de product (activiteit) of meerdere "4" of "4,6".
        last_name = Achternaam van de klant.
        start_date = Begindatum / -tijd van de afspraak.
        end_date = Einddatum / -tijd van de afspraak.
        birth_date = Geboortedatum van de klant.
        client_verified = Indicatie of de klant zich heeft geverifieerd.
        client_recurring = Indicatie of de klant een terugkerende afspraak wil maken.
        client_mail = Emailadres van de klant.

        returns:
        appointment_details = Dictionary met de afspraakdetails.
        """
        appointment_details = {}
        appointment_details.update(locationID=location_id,
                                    productID=product_id,
                                    clientLastName=last_name,
                                    appStartTime=start_date,
                                    clientDateOfBirth=birth_date,
                                    appEndTime=end_date,
                                    isClientVerified = client_verified,
                                    isRecurring = client_recurring,
                                    clientMail = client_mail)
    
    
        return appointment_details  

    def make_appointment(self, app_detail_obj):
        """
        params:
        app_detail_obj = contains all details to book an appointment.

        returns:
        een object met de response.
        Indien updateStatus = 0, dan wordt hier het afspraaknummer van de 
        succesvol toegevoegde afspraak teruggegeven.
        """
        client = Client(wsdl=path_to_wdsl, settings=settings)
        zeep_object = client.service.bookGovAppointment(appDetail=app_detail_obj)
        response = helpers.serialize_object(zeep_object, dict)
        
        return response

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {"product": [self.from_text()],
                "afspraak_datetime": [self.from_text()],
                "achternaam": [self.from_text()],
                "geboortedatum": [self.from_text()],
                "email": [self.from_text()],
                "locatie": [self.from_text()],
                }

    def format_period(self, entities):
        """
        params:
        entities = tracker.latest_message.get("entities", [])
        
        output:
        dictionary containing time and period
        else: returns empty dict
        example: {'17:00': 'PM', '05:00': 'AM'}
        example: {}
        """
        
        # init dict
        time_period = {}
        # check if list and list not empty
        if isinstance(entities, list) and entities:
            # loop in entities list
            for entity in entities:
                # extract entity extractor
                entity_extractor = entity.get("extractor")
                # check if Duckling extractor
                if entity_extractor == "DucklingEntityExtractor":
                    # get additional_info
                    additional_info = entity.get("additional_info").get("values")
                    # loop in additional info
                    for dictionary in additional_info:
                        # convert value to datetime object
                        date = datetime.fromisoformat(dictionary.get("value"))
                        # get period
                        format_time = date.strftime('%H:%M %p')
                        # get time and period
                        time, period = format_time.split(" ")
                        # update dictionary
                        time_period[time] = period
        
        return time_period

    def format_datetime(self, datetime_object, time_only: Optional[bool] = False, day_only: Optional[bool] = False):
        "Ontvangt een datetime object en maakt er een string met betere opmaak van"
        dagid_to_str = {0:'maandag', 1:"dinsdag", 2:"woensdag", 3:"donderdag", 4:'vrijdag', 5:"zaterdag", 6: "zondag"}
        month_to_str = {1:'januari', 2:"februari", 3:"maart", 4:"april", 5:"mei", 6:"juni", 7:"juli", 8:"augustus", 
        9:"september", 10: "oktober", 11:'november', 12:"december"}

        dag_id = datetime_object.weekday()
        dag_str = datetime_object.day
        weekdag = dagid_to_str[dag_id]
        uur = datetime_object.hour
        minuut = datetime_object.minute
        maand_id = datetime_object.month
        maand_str = month_to_str[maand_id] 
        jaar = datetime_object.year

        formatted_time = f"{weekdag} {dag_str} {maand_str} om {uur}:{minuut:02d}"

        if time_only:
            formatted_time = f"{uur}:{minuut:02d}"

        if day_only:
            formatted_time = f"{weekdag} {dag_str} {maand_str}"

        return formatted_time


    def request_next_slot(
        self,
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[EventType]]:
        """Request the next slot and utter template if needed,
        else return None"""

        product = tracker.get_slot("product")

        # Check if product is not None and product in prod mapping
        if product and product in PRODMAPPING:
            product_id = PRODMAPPING[product]["productid"]
            product_desc = PRODMAPPING[product]["productdesc"]

        if tracker.get_slot("email") != None:
            dispatcher.utter_message(template="utter_bevestiging_afspraak")
            return [AllSlotsReset(), self.deactivate()]

        # loop in required slots
        for slot in self.required_slots(tracker):
            # check if slot needs to be requested
            if self._should_request_slot(tracker, slot):
                # check if fallback not equal to True
                if tracker.get_slot("fallback") != True:
                    # dispatch utter_ask
                    dispatcher.utter_message(template=f"utter_ask_{slot}", **tracker.slots)
                    # check if locatie and product is given and product in prod mapping
                    if slot == "locatie" and product and product in PRODMAPPING:
                        # get list containing location
                        location_name_list = client.get_locations_and_earliest_timeslot([AppointmentProduct(identifier=product_id, 
                                                                                                            name=product_desc)])
                        # init dict
                        location_dict = {}
                        #TODO: transform to comprehension list
                        # loop through location objects
                        for idx, _ in enumerate(location_name_list):
                            # get values
                            location_name = location_name_list[idx][0].name
                            location_time = location_name_list[idx][1]
                            # add to dictionary
                            location_dict[location_name] = location_time
                        
                        location_string = ""
                        for name, time in location_dict.items():
                            formatted_time = self.format_datetime(time)

                            location_string += f"\n - **{name}**, vanaf {formatted_time}"
                        
                        logger.info("location_string: {}".format(location_string))
                        dispatcher.utter_message(location_string)
                        dispatcher.utter_message(template="utter_ask_locatie2")
                        
                    if slot == "afspraak_datetime" and product and product in PRODMAPPING:

                        location = tracker.get_slot("locatie")
                        locationID = tracker.get_slot("locationID")

                        time_list = client.get_timeslots_for_upcoming_days([AppointmentProduct(identifier=product_id, name=product_desc)], 
                                                                            AppointmentLocation(identifier=locationID, name=location), num_samples = 2)
                        logger.info("time list: {}".format(time_list))

                        time_string = ""

                        loop_number = 0
                        for x in time_list:
                            while loop_number < 2:
                                first_time = self.format_datetime(x[1][0])
                                second_time = self.format_datetime(x[1][1])

                                time_string += "\n - **{}**".format(first_time)
                                time_string += "\n - **{}**".format(second_time)
                                loop_number += 1
                            
                        dispatcher.utter_message(time_string) 
                        dispatcher.utter_message(template="utter_ask_afspraak_datetime2")


                    return [SlotSet(REQUESTED_SLOT, slot)]

        # no more required slots to fill
        return [SlotSet("fallback", False)]

    async def validate_product(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        intent = tracker.latest_message.get('intent')['name']
        product = next(tracker.get_latest_entity_values("product"), None)
        logger.info("PRODUCT")
        

        if product is None:
            dispatcher.utter_message(template="utter_fallback_product")
            return {"product": None, "fallback": True, "fallback_num": 1}
        if product == "paspoort" or product == "id-kaart" or product == "rijbewijs":
            return {"product": product, "fallback_num": 0}
        else:
            dispatcher.utter_message()
            return {"product": None, "fallback": True, "fallback_num": 1}

    async def validate_locatie(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        logger.info("LOCATIE")

        product = tracker.get_slot("product")

        # Check if product is not None and product in prod mapping
        if product and product in PRODMAPPING:
            product_id = PRODMAPPING[product]["productid"]
            product_desc = PRODMAPPING[product]["productdesc"]

        logger.info("VALUE: {}".format(value))
        pred_location = self.detect_location(value, product_id = product_id, 
                                             product_description = product_desc)
                                             
        locatie = pred_location.get("location", None)
        locationID = pred_location.get("locationID", None)
        # temp logs
        logger.info("pred_location: {}".format(pred_location))

        if locatie:
            return {"locatie": locatie, "locationID": locationID, "fallback_num": 0}
        else:
            dispatcher.utter_message(template="utter_fallback_locatie")
            return {"locatie": None, "fallback": True, "fallback_num": 1}


    async def validate_afspraak_datetime(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:

        product = tracker.get_slot("product")

        # Check if product is not None and product in prod mapping
        if product and product in PRODMAPPING:
            product_id = PRODMAPPING[product]["productid"]
            product_desc = PRODMAPPING[product]["productdesc"]

        fallback_num = tracker.get_slot("fallback_num")
        time = next(tracker.get_latest_entity_values("time"), None)
        location = tracker.get_slot("locatie")
        locationID = tracker.get_slot("locationID")
        logger.info("TIME: {}".format(next(tracker.get_latest_entity_values("time"), None)))

        logger.info("TYPE TIME: {}".format(type(time)))


        # if time is a range
        if isinstance(time, dict):
    
            logger.info("time: {}".format(time))
            end = str(time.get("to", None))
            start = str(time.get("from", None))
            logger.info("TO: {}".format(end))
            logger.info("START: {}".format(start))

            if end != "None":
                end = datetime.fromisoformat(end).replace(tzinfo=None)

                entities = tracker.latest_message.get("entities")
                logger.info("ENTITEITEN: {}".format(entities))

                try:
                    grain = entities[-1]["additional_info"]["values"][0]["to"]["grain"]
                    logger.info("grain hier: {}".format(grain))

                    if grain == "hour":
                        end = end - timedelta(hours=1)

                except:
                    logger.info("no grain found")

            if start != "None":
                start = datetime.fromisoformat(start).replace(tzinfo=None)
            if start == "None":
                start = str(datetime.now())
                start = datetime.fromisoformat(start).replace(tzinfo=None)

            timeslots = client.get_timeslots_for_dates([AppointmentProduct(identifier=product_id, name=product_desc)],AppointmentLocation(identifier=locationID, name=location),start,end)
            logger.info("TIMESLOTS: {}".format(timeslots))
            timeslots = timeslots[0][1]
            try:
                weekday = self.format_datetime(timeslots[0], False, True)
            except:
                weekday = "deze dag"
            logger.info("timeslots: {}".format(timeslots))
            

            time_string = "Op {} heb ik de volgende opties voor je:".format(weekday)

            nr = 0
            logger.info('random stufff')
            while nr < len(timeslots):
                formatted_time = self.format_datetime(timeslots[nr], True)
                logger.info(f'timeslots1: {timeslots}')
                time_string += "\n - **{}**".format(formatted_time)
                nr += 5

            dispatcher.utter_message(time_string)
            return {"time": None, "date_context": str(time.get("from", None)), "fallback": True, "fallback_num": 1}

        # Functie met range zoeker implementeren

        if time is None:
            dispatcher.utter_message(template="utter_fallback_time")
            return {"time": None, "fallback": True, "fallback_num": 1}
        
        elif time and product in PRODMAPPING:
            # Transforming provided time to ISO (mandatory for JCC call)
            iso_time = datetime.fromisoformat(str(tracker.get_slot("time")))

            # Extracting 'grain', 'from' and 'to' from user's time input to decide follow-up
            entities = tracker.latest_message.get("entities")
            logger.info("entities: {}".format(entities))
            grain = entities[-1]["additional_info"]["grain"]
            logger.info("grain: {}".format(grain))

            # Making call to JCC
            time_check = client.get_times([AppointmentProduct(identifier=product_id, name=product_desc)], 
                                           AppointmentLocation(identifier=locationID, name=location), iso_time)
            
            datetime_time = datetime.fromisoformat(str(iso_time)).replace(tzinfo=None)
            # temp logs
            logger.info("datetime_time:{}".format(datetime_time))
            logger.info("time_check: {}".format(time_check))

            # Grain is minute/hour, so users provided specific date. Check if date + time provided by user is available.
            datetime_newtime = None
            new_datetime = None

            if (grain == "minute" or grain == "hour") and tracker.get_slot("date_context"):
                logger.info("IN NIEUWE MINUTE")
                logger.info("time before context: {}".format(time))
                logger.info("date_context: {}".format(tracker.get_slot("date_context")))

                time = datetime.fromisoformat(time)
                context_date = datetime.fromisoformat(tracker.get_slot("date_context"))
                new_time = time.time()
                new_date = context_date.date()
                new_datetime = datetime.combine(new_date, new_time)
                
                datetime_newtime = str(new_datetime)
                logger.info("new_datetime: {}".format(new_datetime))

                time_check = client.get_times([AppointmentProduct(identifier=product_id, name=product_desc)], 
                                AppointmentLocation(identifier=locationID, name=location), new_datetime)

            if new_datetime and (grain == "minute" or grain == "hour") and new_datetime in time_check:
                dispatcher.utter_message("Deze plek is nog beschikbaar.")
                return {"afspraak_datetime": datetime_newtime, "fallback_num": 0}
                
            elif datetime_time and (grain == "minute" or grain == "hour") and datetime_time in time_check:
                dispatcher.utter_message("Deze plek is nog beschikbaar.")
                return {"afspraak_datetime": time, "fallback_num": 0}                    
            
            if ((grain == "minute" or grain == "hour") and datetime_time not in time_check) or (datetime_newtime and (grain == "minute" or grain == "hour") and datetime_newtime not in time_check):
                logger.info("IN DE NIET GEVONDEN")
                # Removing timezones from all times

                if datetime_newtime:
                    iso_time = new_datetime.replace(tzinfo=None)

                else:
                    iso_time = iso_time.replace(tzinfo=None)
                for date in time_check:
                   date.replace(tzinfo=None)
                # Vind dichtsbijzijnde datum (To do: meerdere data returnen ipv 1)
                cloz_dict = { 
                abs(iso_time - date) : date 
                for date in time_check}     
                logger.info("cloz dict: {}".format(cloz_dict))               
                min_keys = sorted(list(cloz_dict.keys()))[:4]
                logger.info("min_keys: {}".format(min_keys))
                res = [cloz_dict[key] for key in min_keys]         
                
                logger.info("Nearest date from list : " + str(res))

                if res:
                    dispatcher.utter_message("Deze tijd is helaas niet beschikbaar. De volgende tijden liggen in de buurt.")
                    time_string = ""
                    for x in res:
                        formatted_time = self.format_datetime(x, True)
                        time_string += "\n - **{}**".format(formatted_time)

                    dispatcher.utter_message(time_string)

                    return {"afspraak_datetime": None, "fallback_num": 1, "fallback": True}
            
                if not res:

                    dispatcher.utter_message("Voor deze datum kon ik geen tijden vinden. Hier zijn nog een aantal opties.")

                    location = tracker.get_slot("locatie")
                    locationID = tracker.get_slot("locationID")

                    time_list = client.get_timeslots_for_upcoming_days([AppointmentProduct(identifier=product_id, name=product_desc)], 
                                                                        AppointmentLocation(identifier=locationID, name=location), num_samples = 2)
                    logger.info("time list: {}".format(time_list))

                    time_string = ""

                    for x in time_list:
                        first_time = self.format_datetime(x[1][0])
                        second_time = self.format_datetime(x[1][1])
                        time_string += "\n - **{}**".format(first_time)
                        time_string += "\n - **{}**".format(second_time)
                        
                    dispatcher.utter_message(time_string) 
                    dispatcher.utter_message(template="utter_ask_afspraak_datetime2")
                    return {"afspraak_datetime": None, "fallback_num": 1, "fallback": True}

            # Grain is day, so we'll provide him with some options this particular day.
            if grain == "day":

                if not time_check:
                    dispatcher.utter_message("Op deze dag zijn er helaas geen afspraken mogelijk.")
                    return {"afspraak_datetime": None, "date_context": time, "fallback_num": 1, "fallback": True}

                weekday = self.format_datetime(time_check[0], False, True)
                dispatcher.utter_message("Voor {} heb ik de volgende tijden gevonden:".format(weekday)) 

                time_string = ""

                loop_number = 0
                logger.info('in the day loop')
                while loop_number < len(time_check):
                    formatted_time = self.format_datetime(time_check[loop_number], True)
                    time_string += "\n - **{}**".format(formatted_time)
                    loop_number += 5
                    
                dispatcher.utter_message(time_string) 
                return {"afspraak_datetime": None, "date_context": time, "fallback_num": 1, "fallback": True}

        else:
            pass
        
    async def validate_achternaam(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        logger.info("ACHTERNAAM: {}".format(value))

        if value:
            return {"achternaam": value, "fallback_num": 0}
        else:
            return {"achternaam": None, "fallback": True, "fallback_num": 1}

    async def validate_geboortedatum(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        logger.info("Geboortedatum: {}".format(value))
        time = next(tracker.get_latest_entity_values("time"), None)
        
        if time:
            return {"geboortedatum": time, "fallback_num": 0}
        else:
            dispatcher.utter_message(template="utter_fallback_geboortedatum")
            return {"geboortedatum": None, "fallback": True, "fallback_num": 1}

            

    async def validate_email(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        email = next(tracker.get_latest_entity_values("email"), None)


        if email:

                ## Assemble the appointment API payload variables.
                start_time = datetime.fromisoformat(tracker.get_slot("afspraak_datetime"))
                end_time = start_time + timedelta(hours=1)
                locatie = tracker.get_slot("locatie")
                product = tracker.get_slot("product")
                achternaam = tracker.get_slot("achternaam")
                birth_date = datetime.fromisoformat(tracker.get_slot("geboortedatum"))
                location_id = tracker.get_slot("locationID")
                if product and product in PRODMAPPING:
                    product_id = PRODMAPPING[product]["productid"]

                # temp logs
                logger.info("last_name: {}".format(achternaam))
                logger.info("start_time: {}".format(start_time))
                logger.info("end_time: {}".format(end_time))
                logger.info("birth_date: {}".format(birth_date))
                logger.info("email: {}".format(email))

                #TODO: replace with JCCClient
                # Create the appointment details object.
                app_details = self.create_appointment_details(location_id = location_id, 
                                                        product_id = product_id,
                                                        last_name = achternaam, 
                                                        start_date = start_time, 
                                                        end_date = end_time,
                                                        birth_date = birth_date,
                                                        client_verified = "0",
                                                        client_recurring = "false",
                                                        client_mail = email)
                
                # temp logs
                logger.info("app_details: {}".format(app_details))

                #TODO: replace with JCCClient
                # make appointment
                app_response = self.make_appointment(app_details)


                # temp logs
                logger.info("make appointment response: {}".format(app_response))

                # Check the statuscode of JCC and act upon that
                if app_response:
                    status = app_response.get("updateStatus", None)
                    logger.info("status: {}".format(status))
                    logger.info(type(status))
        
                    if status in CODEMAPPING and status not in [0, 5]:
                        dispatcher.utter_message("Er is helaas iets mis gegaan.")
                        return {"email": None, "fallback": True, "fallback_num": 2}
                    
                    if status in CODEMAPPING and status == 5:
                        dispatcher.utter_message("Deze datum en tijd is helaas niet meer beschikbaar.")
                        return {"email": None, "afspraak_datetime": None, "fallback_num": 0}
                        
                    if status in CODEMAPPING and status == 0:
                        dispatcher.utter_message("De afspraak staat. Je afspraaknummer is {}".format(app_response.get("appID")))
                        return {"email": email, "fallback_num": 0}

                
        else:
            dispatcher.utter_message(template="utter_fallback_email")
            return {"email": None, "fallback": True, "fallback_num": 1}

    async def submit(
        self,
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> List[EventType]:
        """Define what the form has to do
        after all required slots are filled"""

        return []
        raise NotImplementedError("A form must implement a submit method")

