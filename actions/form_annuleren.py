from typing import Dict, Text, Any, List, Optional, Union

from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, EventType, ActiveLoop, AllSlotsReset
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction, FormAction, REQUESTED_SLOT
import logging
from zeep import Client, Settings, helpers
from datetime import datetime, timedelta
from fuzzywuzzy import fuzz
import os
import json

from .appointments.client import *
from .appointments.jcc import JCCClient

logger = logging.getLogger(__name__)

token = os.getenv('API_TOKEN')
client = JCCClient(token=token)


directory = os.getcwd()
path_to_wdsl = "/app/actions/WSDL_afspraak_maken.wsdl"
settings = Settings(extra_http_headers={'Authorization': 'Basic ' + token})

class ValidateForm(FormAction):
    """Example of a form validation action."""

    def name(self) -> Text:
        return "form_annuleren"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        required_slots = ["afspraaknummer", "email", "annuleringsbevestiging"]
        logger.info("required_slots: {}".format(required_slots))
        return required_slots


    def create_appointment_details(self, location_id: str, product_id: str, 
                               last_name: str, start_date, end_date,
                               birth_date, client_verified, client_recurring,
                               client_mail: str):

    
        appointment_details = {}
        appointment_details.update(locationID=location_id)
    
    
        return appointment_details  

    def make_appointment(self, app_detail_obj):
        """
        params:
        app_detail_obj = contains all details to book an appointment.

        returns:
        een object met de response.
        Indien updateStatus = 0, dan wordt hier het afspraaknummer van de 
        succesvol toegevoegde afspraak teruggegeven.
        """
        client = Client(wsdl=path_to_wdsl, settings=settings)
        zeep_object = client.service.bookGovAppointment(appDetail=app_detail_obj)
        response = helpers.serialize_object(zeep_object, dict)
        
        return response

    def format_datetime(self, datetime_object):
        "Ontvangt een datetime object en maakt er een string met betere opmaak van"
        dagid_to_str = {0:'maandag', 1:"dinsdag", 2:"woensdag", 3:"donderdag", 4:'vrijdag', 5:"zaterdag", 6: "zondag"}
        month_to_str = {1:'januari', 2:"februari", 3:"maart", 4:"april", 5:"mei", 6:"juni", 7:"juli", 8:"augustus", 
        9:"september", 10: "oktober", 11:'november', 12:"december"}

        dag_id = datetime_object.weekday()
        dag_str = datetime_object.day
        weekdag = dagid_to_str[dag_id]
        uur = datetime_object.hour
        minuut = datetime_object.minute
        maand_id = datetime_object.month
        maand_str = month_to_str[maand_id] 
        jaar = datetime_object.year
        formatted_time = f"{weekdag} {dag_str} {maand_str} {jaar} om {uur}:{minuut:02d}"
        return formatted_time


    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {"email": [self.from_text()],
                "afspraaknummer": [self.from_text()],
                "annuleringsbevestiging": [self.from_text()]
                }

    def request_next_slot(
        self,
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> Optional[List[EventType]]:
        """Request the next slot and utter template if needed,
        else return None"""
        # loop in required slots
        for slot in self.required_slots(tracker):
            # check if slot needs to be requested
            if self._should_request_slot(tracker, slot):
                # check if fallback not equal to True
                if tracker.get_slot("fallback") != True:
                    # dispatch utter_ask
                    dispatcher.utter_message(template=f"utter_ask_{slot}", **tracker.slots)
                    return [SlotSet(REQUESTED_SLOT, slot)]

        # no more required slots to fill
        return [SlotSet("fallback", False)]

    async def validate_afspraaknummer(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        intent = tracker.latest_message.get('intent')['name']
        number = next(tracker.get_latest_entity_values("number"), None)
        logger.info("NUMBER: {}".format(number))
        logger.info("AFSPRAAKNUMMER")
        

        if number:
            try:
                app_details = client.get_appointment_details(str(number))

                if app_details:
                    dispatcher.utter_message("Details gevonden!")

                else:
                    return {"afspraaknummer": None, "fallback_num": 0}

                return {"afspraaknummer": number, "fallback_num": 0}

            except Exception as e:
                logger.info("EXCEPTION: {}".format(e))
                dispatcher.utter_message(template="utter_fallback_afspraaknummer")
                return {"afspraaknummer": None, "fallback_num": 1, "fallback": True}
        
        else:
            return {"afspraaknummer": None, "fallback_num": 0}


    async def validate_email(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        number = tracker.get_slot("afspraaknummer")
        email = next(tracker.get_latest_entity_values("email"), None)

        if email:
            app_details = client.get_appointment_details(str(number))
            logger.info("app details: {}".format(app_details))
            datum = app_details.start_at
            datum = self.format_datetime(datum)
            datum = str(datum)

            email_to_match = app_details.client.email

            if email == email_to_match:
                dispatcher.utter_message("Dit emailadres is correct.")
                return {"email": email, "afspraak_datetime": datum, "fallback_num": 0}

            else:
                dispatcher.utter_message("geen email match!")
                return {"email": None, "fallback_num": 0}

            return {"email": email, "fallback_num": 0}
        else:
            dispatcher.utter_message(template="utter_fallback_email")
            return {"product": None, "fallback": True, "fallback_num": 1}

    async def validate_annuleringsbevestiging(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        fallback_num = tracker.get_slot("fallback_num")
        intent = tracker.latest_message.get('intent')['name']


        if intent == "inform_ja":
            dispatcher.utter_message("Oke, ik heb de afspraak verwijderd.")
            afspraaknummer = tracker.get_slot("afspraaknummer")

            result = client.delete_appointment(str(afspraaknummer))
            logger.info("result; {}".format(result))
            return {"annuleringsbevestiging": "ja", "fallback_num": 0}

        if intent == "inform_nee":
            dispatcher.utter_message("Oke, de annulering is gecanceld.")
            return {"annuleringsbevestiging": "nee", "fallback_num": 0}


        else:
            dispatcher.utter_message(template="utter_fallback_annuleringsbevestiging")
            return {"annuleringsbevestiging": None, "fallback_num": 0}



    async def submit(
        self,
        dispatcher: "CollectingDispatcher",
        tracker: "Tracker",
        domain: "DomainDict",
    ) -> List[EventType]:
        """Define what the form has to do
        after all required slots are filled"""

        return []
        raise NotImplementedError("A form must implement a submit method")

