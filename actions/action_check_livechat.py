import os
import requests
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, UserUtteranceReverted, ActionReverted
from rasa_sdk.interfaces import ActionExecutionRejection
from rasa_sdk import Tracker
import logging
from .constants import DEFAULT_REQUEST_TIMEOUT
logger = logging.getLogger(__name__)



class ActionCheckLivechat(Action):

    availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}"
    livecom_adapter_url = os.getenv("LIVECOM_ADAPTER_URL", "http://web/test-livechat/callback")
    
    # TODO should use NLX
    adapter_mapping = {
        "Dordrecht": livecom_adapter_url.format(municipality="dordrecht"),
        "Sliedrecht": livecom_adapter_url.format(municipality="sliedrecht"),
        "Hendrik-Ido-Ambacht": livecom_adapter_url.format(municipality="hendrik"),
        "Zwijndrecht": livecom_adapter_url.format(municipality="zwijndrecht"),
        "Alblasserdam": livecom_adapter_url.format(municipality="alblasserdam"),
    }

    def name(self):
        return "action_check_livechat"

    def run(self, dispatcher, tracker, domain):
        # precondition municipality set
        municipality = tracker.get_slot("municipality")

        if not municipality:

            events = tracker.events[::-1]
            user_event_metadata = None
            metadata_municipality = None
            for user_event in events:
                if user_event["event"] == "user" and user_event.get("metadata", None):
                    user_event_metadata = user_event["metadata"]
                    break  
            
            if user_event_metadata:
                municipality = user_event_metadata.get("municipality", None)

            else:
                municipality = "Tilburg"
                # raise ActionExecutionRejection(self.name(),f"no municipality set, use check_municipality in story / prediction first")

        events = tracker.events[::-1]

        user_event_metadata = None
        for user_event in events:
            if user_event["event"] == "user" and user_event.get("metadata", None):
                user_event_metadata = user_event["metadata"]
                break  

        if not user_event_metadata:
            return [SlotSet("feature_livechat","niet_aangesloten")]

        widget_livechat_type = user_event_metadata.get("livechat")
        widget_livechat_guid = user_event_metadata.get("guid")
        widget_municipality = user_event_metadata.get("municipality")
        skip_router = user_event_metadata.get("skipRouter", False)

        logger.info(f"Chat via router: {not skip_router}")

        if user_event.get("input_channel") == "test":
            return [SlotSet("feature_livechat", user_event_metadata.get("livechat"))]

        if widget_livechat_type == "obi4wan" and widget_municipality == municipality and widget_livechat_guid:
            try:
                availability_resp = requests.get(
                    self.availability_uri.format(widget_livechat_guid), 
                    headers={"accept": "application/json"},
                    timeout=DEFAULT_REQUEST_TIMEOUT
                ).json()
            except requests.exceptions.RequestException as e:
                logger.error(f"Error occurred while requesting availability API: {e}")
            else:
                availability = availability_resp['available']
                if availability:
                    logger.info("OBI4wan: Agent available") # DEBUG
                    return [SlotSet("feature_livechat","open")]
                else:
                    logger.info("OBI4wan: Agent NOT available") # DEBUG
                    return [SlotSet("feature_livechat","gesloten")]
        elif not skip_router and municipality in self.adapter_mapping:
            try:
                availability_resp = requests.get(
                    self.adapter_mapping[municipality],
                    headers={"accept": "application/json"},
                    timeout=DEFAULT_REQUEST_TIMEOUT
                ).json()
            except requests.exceptions.RequestException as e:
                logger.error(f"Error occurred while requesting status API: {e}")
            else:
                status = availability_resp['status']
                if status == "ok":
                    logger.info("Livechat adapter available") # DEBUG
                    return [SlotSet("feature_livechat","open")]
                else:
                    logger.info("Livechat adapter not available") # DEBUG
                    return [SlotSet("feature_livechat","gesloten")]
        return [SlotSet("feature_livechat","niet_aangesloten")]
