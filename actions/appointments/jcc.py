# Adapted from Open Forms
# https://github.com/open-formulieren/open-forms
import logging
import random
from dataclasses import dataclass
from datetime import date, datetime, timedelta
from typing import Dict, List, Optional, Tuple, Union

from requests.exceptions import RequestException
from zeep import Client, Settings, helpers
from zeep.exceptions import Error as ZeepError

# from ...utils import create_base64_qrcode
from .client import (AppointmentClient, AppointmentDetails,
                     AppointmentLocation, AppointmentProduct,
                     BaseAppointmentClient)
from .exceptions import (AppointmentCreateFailed, AppointmentDeleteFailed,
                         AppointmentException)
from .utils import create_datetime_range

logger = logging.getLogger(__name__)


def squash_ids(lst):
    return ",".join([i.identifier for i in lst])


@dataclass
class JCCClient(BaseAppointmentClient):
    def __init__(self, token=None):
        # TODO should be configurable
        path_to_wdsl = "/app/actions/WSDL_afspraak_maken.wsdl"
        settings = Settings(extra_http_headers={"Authorization": "Basic " + token})
        self.client = Client(wsdl=path_to_wdsl, settings=settings)
        # TODO should use redis
        self._location_cache = {}

    def get_available_products(
        self, current_products: Optional[List[AppointmentProduct]] = None
    ) -> List[AppointmentProduct]:
        try:
            if current_products:
                current_product_ids = squash_ids(current_products)
                result = self.client.service.getGovAvailableProductsByProduct(
                    ProductNr=current_product_ids
                )
            else:
                result = self.client.service.getGovAvailableProducts()
        except (ZeepError, RequestException) as e:
            logger.exception("Could not retrieve available products", exc_info=e)
            return []
        except Exception as exc:
            raise AppointmentException from exc

        return [
            AppointmentProduct(
                entry["productId"], entry["productDesc"], entry["productCode"]
            )
            for entry in result
        ]

    def get_locations(
        self, products: List[AppointmentProduct]
    ) -> List[AppointmentLocation]:
        product_ids = squash_ids(products)

        try:
            result = self.client.service.getGovLocationsForProduct(
                productID=product_ids
            )
        except (ZeepError, RequestException) as e:
            logger.exception(
                "Could not retrieve locations for products '%s'",
                product_ids,
                exc_info=e,
            )
            return []
        except Exception as exc:
            raise AppointmentException from exc

        locations = []
        for entry in result:
            location_id = entry["locationID"]
            if location_id in self._location_cache:
                location = self._location_cache[location_id]
            else:
                location = self.client.service.getGovLocationDetails(
                    locationID=location_id
                )
                self._location_cache[location_id] = location
            locations.append(
                AppointmentLocation(
                    location_id,
                    location["locationDesc"],
                    location["address"],
                    location["postalcode"],
                    location["city"],
                    location["telephone"],
                )
            )
        return locations

    def get_dates(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        start_at: Optional[date] = None,
        end_at: Optional[date] = None,
    ) -> List[date]:
        product_ids = squash_ids(products)

        start_at = start_at or date.today()
        end_at = end_at or (start_at + timedelta(days=14))

        try:
            max_end_date = self.client.service.getGovLatestPlanDate(
                productId=product_ids
            )
            if end_at > max_end_date:
                end_at = max_end_date

            days = self.client.service.getGovAvailableDays(
                locationID=location.identifier,
                productID=product_ids,
                startDate=start_at,
                endDate=end_at,
                appDuration=0,
            )
            return days
        except (ZeepError, RequestException) as e:
            logger.exception(
                "Could not retrieve dates for products '%s' at location '%s' between %s - %s",
                product_ids,
                location,
                start_at,
                end_at,
                exc_info=e,
            )
            return []
        except Exception as exc:
            raise AppointmentException from exc

    def get_times(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        day: date,
    ) -> List[datetime]:
        product_ids = squash_ids(products)

        try:
            times = self.client.service.getGovAvailableTimesPerDay(
                date=day,
                productID=product_ids,
                locationID=location.identifier,
                appDuration=0,
            )
            return times
        except (ZeepError, RequestException) as e:
            logger.exception(
                "Could not retrieve times for products '%s' at location '%s' on %s",
                product_ids,
                location,
                day,
                exc_info=e,
            )
            return []
        except Exception as exc:
            raise AppointmentException from exc

    def get_timeslots_for_dates(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        begin_date: Union[datetime, date],
        end_date: Union[datetime, date],
    ) -> List[Tuple[date, List[datetime]]]:
        timeslots_per_day = {}
        date_range = create_datetime_range(begin_date, end_date)
        for i, day_or_datetime in enumerate(date_range):
            day = (
                day_or_datetime.date()
                if isinstance(day_or_datetime, datetime)
                else day_or_datetime
            )
            times = self.get_times(products, location, day)
            if isinstance(day_or_datetime, datetime):
                if not i:
                    times = [time for time in times if time >= day_or_datetime]
                if i == len(date_range) - 1:
                    times = [time for time in times if time < day_or_datetime]
            existing_timeslots = timeslots_per_day.get(day, [])
            if existing_timeslots:
                times = [time for time in times if time in existing_timeslots]
            timeslots_per_day[day] = times
        return [
            (
                k,
                v,
            )
            for k, v in timeslots_per_day.items()
        ]

    def get_locations_and_earliest_timeslot(
        self, products: List[AppointmentProduct]
    ) -> List[Tuple[AppointmentLocation, datetime]]:
        locations = self.get_locations(products)

        locations_and_timeslots = []
        for location in locations:
            earliest_possible_date = self.get_dates(products, location)[0]
            earliest_possible_time = self.get_times(
                products, location, earliest_possible_date
            )[0]
            locations_and_timeslots.append(
                (
                    location,
                    earliest_possible_time,
                )
            )
        return sorted(locations_and_timeslots, key=lambda x: x[1])

    def get_timeslots_for_upcoming_days(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        num_samples: int = 2,
    ) -> List[Tuple[AppointmentLocation, List[datetime]]]:
        dates = self.get_dates(products, location)[:5]

        date_timeslots = []
        for date in dates:
            times = self.get_times(products, location, date)
            num_samples = num_samples if len(times) >= num_samples else len(times)
            sampled_times = sorted(random.sample(times, k=num_samples))
            date_timeslots.append((date, sampled_times))

        return date_timeslots

    def create_appointment(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        start_at: datetime,
        client: AppointmentClient,
        remarks: str = "",
    ) -> str:
        product_ids = squash_ids(products)

        try:
            factory = self.client.type_factory("http://www.genericCBS.org/GenericCBS/")
            appointment_details = factory.AppointmentDetailsType(
                locationID=location.identifier,
                productID=product_ids,
                clientLastName=client.last_name,
                clientDateOfBirth=client.birthdate,
                appStartTime=start_at,
                appEndTime=start_at,  # Required but unused by the service.
                isClientVerified=False,
                isRecurring=False,
                # Phone number is often required for appointment,
                # use fake phone number if no client phone number
                clientTel=client.phonenumber or "0123456789",
                # Optional fields.
                # These might be needed. Depends on `GetRequiredClientFields`
                #
                # clientID=bsn,
                # clientSex="M/F",
                # clientInitials="",
                # clientAddress="",
                # clientPostalCode="",
                # clientCity="",
                # clientCountry="",
                # clientMail="",
                appointmentDesc=remarks,
                # caseID": "",
            )

            result = self.client.service.bookGovAppointment(
                appDetail=appointment_details, fields=[]
            )

            if result["updateStatus"] == 0:
                return result["appID"]
            else:
                raise AppointmentCreateFailed(
                    "Could not create appointment for products '%s' at location '%s' starting at %s (updateStatus=%s)",
                    product_ids,
                    location,
                    start_at,
                    result["updateStatus"],
                )
        except (ZeepError, RequestException, KeyError) as e:
            raise AppointmentCreateFailed(e)

    def delete_appointment(self, identifier: str) -> None:
        try:
            result = self.client.service.deleteGovAppointment(appID=identifier)

            if result != 0:
                raise AppointmentDeleteFailed(
                    "Could not delete appointment: %s (updateStatus=%s)",
                    identifier,
                    result,
                )
        except (ZeepError, RequestException) as e:
            raise AppointmentDeleteFailed(e)

    def get_appointment_details(self, identifier: str) -> str:
        try:
            # NOTE: The operation `getGovAppointmentExtendedDetails` seems
            # missing. This would include the product descriptions but now we
            # need to make an additional call to get those.
            details = self.client.service.getGovAppointmentDetails(appID=identifier)
            if details is None:
                raise AppointmentException("No appointment details could be retrieved.")

            location = self.client.service.getGovLocationDetails(
                locationID=details.locationID
            )
            qrcode = self.client.service.GetAppointmentQRCodeText(appID=identifier)

            app_products = []
            for pid in details.productID.split(","):
                product = self.client.service.getGovProductDetails(productID=pid)

                app_products.append(
                    AppointmentProduct(identifier=pid, name=product.description)
                )

            # qrcode_base64 = create_base64_qrcode(qrcode)

            # qr_label = "QR-code"
            # # qr_value = format_html(
            # #     '<img src="data:image/png;base64,{qrcode_base64}" alt="{qrcode}" />',
            # #     qrcode_base64=qrcode_base64,
            # #     qrcode=qrcode,
            # # )
            # qr_value = '<img src="data:image/png;base64,{qrcode_base64}" alt="{qrcode}" />'

            result = AppointmentDetails(
                identifier=identifier,
                products=app_products,
                location=AppointmentLocation(
                    identifier=details.locationID,
                    name=location.locationDesc,
                    address=location.address,
                    postalcode=location.postalcode,  # Documentation says `postalCode`
                    city=location.city,
                ),
                client=AppointmentClient(
                    last_name=details.clientLastName,
                    birthdate=details.clientDateOfBirth,
                    initials=details.clientInitials,
                    phonenumber=details.clientTel,
                    email=details.clientMail,
                ),
                start_at=details.appStartTime,
                end_at=details.appEndTime,
                remarks=details.appointmentDesc,
                # other={qr_label: qr_value},
            )

            return result

        except (ZeepError, RequestException, AttributeError) as e:
            raise AppointmentException(e)
