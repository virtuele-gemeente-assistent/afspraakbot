from datetime import date, datetime, time, timedelta
from typing import Union


# begin, end = (datetime(2022, 6, 27, 12,0,0), datetime(2022, 6, 27, 15,0,0),)
def create_datetime_range(begin: Union[datetime, date], end: Union[datetime, date]):
    begin_is_datetime = isinstance(begin, datetime)
    end_is_datetime = isinstance(end, datetime)
    first = begin
    last = end
    if begin_is_datetime ^ end_is_datetime:
        if begin_is_datetime:
            last = datetime.combine(end, time())
        else:
            first = datetime.combine(begin, time())

    num_days = (last - first).days or 1
    date_range = [begin + timedelta(days=x) for x in range(num_days)] + [end]

    return date_range


# print(create_datetime_range(begin, end))
