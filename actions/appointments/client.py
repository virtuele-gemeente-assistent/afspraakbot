# Adapted from Open Forms
# https://github.com/open-formulieren/open-forms
from abc import ABC
from dataclasses import dataclass
from datetime import date, datetime
from typing import Dict, List, Optional, Tuple, Union


@dataclass()
class AppointmentProduct:
    identifier: str
    name: str
    code: Optional[str] = None

    def __str__(self):
        return self.identifier


@dataclass()
class AppointmentLocation:
    identifier: str
    name: str
    address: Optional[str] = None
    postalcode: Optional[str] = None
    city: Optional[str] = None
    telephone: Optional[str] = None

    def __str__(self):
        return self.identifier


@dataclass()
class AppointmentClient:
    last_name: str
    birthdate: date
    initials: Optional[str] = None
    phonenumber: Optional[str] = None
    email: Optional[str] = None

    def __str__(self):
        return self.last_name


@dataclass()
class AppointmentDetails:
    identifier: str
    products: List[AppointmentProduct]
    location: AppointmentLocation
    start_at: datetime
    end_at: Optional[datetime] = None
    remarks: Optional[str] = None
    client: Optional[AppointmentClient] = None

    # These are typically key/values-pairs where both the key and value are
    # considered to be HTML-safe and suited to show to end users.
    other: Optional[dict] = None

    def __str__(self):
        return self.identifier


class BaseAppointmentClient(ABC):
    @property
    def is_enabled(self):
        # TODO currently not configurable,
        # see https://github.com/open-formulieren/open-forms/issues/1103
        # https://github.com/open-formulieren/open-forms/issues/623 should be
        # implemented to properly set up generic plugin enable/disable behaviour
        return True

    def get_available_products(
        self, current_products: Optional[List[AppointmentProduct]] = None
    ) -> List[AppointmentProduct]:
        """
        Retrieve all available products and services to create an appointment for.

        You can pass ``current_products`` to only retrieve available
        products in combination with the ``current_products``.

        :param current_products: List of :class:`AppointmentProduct`, as obtained from
          another :meth:`get_available_products` call.
        :returns: List of :class:`AppointmentProduct`
        """
        raise NotImplementedError()

    def get_locations(
        self, products: List[AppointmentProduct]
    ) -> List[AppointmentLocation]:
        """
        Retrieve all available locations for given ``products``.

        :param products: List of :class:`AppointmentProduct`, as obtained from
          :meth:`get_available_products`. :returns: List of :class:`AppointmentLocation`
        """
        raise NotImplementedError()

    def get_dates(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        start_at: Optional[date] = None,
        end_at: Optional[date] = None,
    ) -> List[date]:
        """
        Retrieve all available dates for given ``products`` and ``location``.

        :param products: List of :class:`AppointmentProduct`, as obtained from :meth:`get_available_products`.
        :param location: An :class:`AppointmentLocation`, as obtained from :meth:`get_locations`.
        :param start_at: The start :class:`date` to retrieve available dates for. Default: ``date.today()``.
        :param end_at: The end :class:`date` to retrieve available dates for. Default: 14 days after ``start_date``.
        :returns: List of :class:`date`
        """
        raise NotImplementedError()

    def get_times(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        day: date,
    ) -> List[datetime]:
        """
        Retrieve all available times for given ``products``, ``location`` and ``day``.

        :param products: List of :class:`AppointmentProduct`, as obtained from `get_available_products`.
        :param location: An :class:`AppointmentLocation`, as obtained from `get_locations`.
        :param day: A :class:`date` to retrieve available times for.
        :returns: List of available :class:`datetime`.
        """
        raise NotImplementedError()

    def get_timeslots_for_dates(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        begin_date: Union[datetime, date],
        end_date: Union[datetime, date],
    ) -> List[Tuple[date, List[datetime]]]:
        raise NotImplementedError()

    def get_locations_and_earliest_timeslot(
        self, products: List[AppointmentProduct]
    ) -> List[Tuple[AppointmentLocation, datetime]]:
        raise NotImplementedError()

    def get_timeslots_for_upcoming_days(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        num_samples: int = 2,
    ) -> List[Tuple[AppointmentLocation, List[datetime]]]:
        raise NotImplementedError()

    def get_calendar(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        start_at: Optional[date] = None,
        end_at: Optional[date] = None,
    ) -> Dict[date, List[datetime]]:
        """
        Retrieve a calendar.

        WARNING: This default implementation has significant performance issues.
        You can override this function with a more efficient implementation if
        the service supports it.

        :param products: List of :class:`AppointmentProduct`, as obtained from :meth:`get_available_products`.
        :param location: An :class:`AppointmentLocation`, as obtained from :meth:`get_locations`.
        :param start_at: The start :class:`date` to retrieve available dates for. Default: ``date.today()``.
        :param end_at: The end :class:`date` to retrieve available dates for. Default: 14 days after ``start_date``.
        :returns: Dict where each key represents a date and the values is a list of times.
        """
        days = self.get_dates(products, location, start_at, end_at)

        result = {}

        for day in days:
            times = self.get_times(products, location, day)
            result[day] = times

        return result

    def create_appointment(
        self,
        products: List[AppointmentProduct],
        location: AppointmentLocation,
        start_at: datetime,
        client: AppointmentClient,
        remarks: str = None,
    ) -> str:
        """
        Create an appointment.

        :param products: List of :class:`AppointmentProduct`, as obtained from :meth:`get_available_products`.
        :param location: An :class:`AppointmentLocation`, as obtained from :meth:`get_locations`.
        :param start_at: A `datetime` to start the appointment, as obtained from :meth:`get_calendar`.
        :param client: A :class:`AppointmentClient` that holds client details.
        :param remarks: A ``str`` for additional remarks, added to the appointment.
        :returns: An appointment identifier as ``str``.
        :raises AppointmentCreateFailed: If the appointment could not be created.
        """
        raise NotImplementedError()

    def delete_appointment(self, identifier: str) -> None:
        """
        Delete an appointment.

        :param identifier: A string that represents the unique identification of the appointment.
        :raises AppointmentDeleteFailed: If the appointment could not be deleted.
        """
        raise NotImplementedError()

    def get_appointment_details(self, identifier: str) -> str:
        """
        Get appointment details.

        :param identifier: A string that represents the unique identification of the appointment.
        :returns: :class:`AppointmentDetails`.
        """
        raise NotImplementedError()
