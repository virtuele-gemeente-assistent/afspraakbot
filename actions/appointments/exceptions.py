# Adapted from Open Forms
# https://github.com/open-formulieren/open-forms
class AppointmentException(Exception):
    pass


class AppointmentDeleteFailed(AppointmentException):
    pass


class AppointmentCreateFailed(AppointmentException):
    pass


class AppointmentInteractionFailed(AppointmentException):
    pass


class AppointmentRegistrationFailed(AppointmentInteractionFailed):
    pass


class AppointmentUpdateFailed(AppointmentInteractionFailed):
    pass
