import os

from .jcc import JCCClient

CONFIGURATION = {
    "Dordrecht": {"client": JCCClient, "kwargs": {"token": os.getenv("API_TOKEN")}}
}
