from rasa_sdk import Action
from rasa_sdk.events import SlotSet
import logging
from zeep import Client
import requests
from fuzzywuzzy import fuzz

logger = logging.getLogger(__name__)


class ActionDetectLocation(Action):
    def name(self):
        return "action_detect_location"

    def get_endpoint(self, gemeente):
        endpoint_dict = {'utrecht': "https://test.jccsoftware.nl/JCC/G-PLAN%20Test/WARP/GGS2/GenericGuidanceSystem2.asmx?wsdl",'tilburg': None} 
        return endpoint_dict[gemeente.lower()]

    def get_locations(self, gemeente):
        # Create connections with jcc
        endpoint = self.get_endpoint(gemeente)
        client = Client(endpoint)

        #Get ids and use them to get more detailed information. Extract name from detailed information
        location_id_list = client.service.getGovLocations()
        location_name_list = []
        for location_id in location_id_list:
            location_details = client.service.getGovLocationDetails(location_id)
            location_name_list.append(location_details['locationDesc'])
        return location_name_list

    def clean_location_split(self, gemeente_naam, location_split):
        location_split = location_split.replace('gemeente', '').strip()
        location_split = location_split.replace(gemeente_naam, '').strip()
        return location_split

    def fuzzy_check(self, location_split, sentence):
        Ratio = fuzz.partial_ratio(location_split,sentence)
        if Ratio > 80:
            return True
        else:
            return False

    def run(self, dispatcher, tracker, domain):
        gemeente_naam = 'utrecht' #hard coded for now. We will extract the name of the gemeente from slots later
        sentence = 'hallo ik wil graag een afspraak op de amazonedreef' #TODO: should be the last sentence from the user

        location_name_list = self.get_locations(gemeente_naam)
        detected_location = None

        for location_name in location_name_list:
            splitted_location_name = location_name.lower().split(',')

            # Loop over all the splits, we do this as we saw that many of the names have the municipality before a comma
            for location_split in splitted_location_name:
                new_detected_location = None
                location_split = self.clean_location_split(gemeente_naam, location_split)
                if len(location_split) <= 1:
                    continue
                
                # Check whether there is an exact match, if not check for a fuzzy match
                if location_split in sentence:
                    new_detected_location = location_name
                elif self.fuzzy_check(location_split, sentence):
                    new_detected_location = location_name

                # Add to the detected_location list
                if new_detected_location != None and detected_location == None:
                    detected_location = [location_name]
                elif new_detected_location != None:
                    detected_location.append(location_name)

        if detected_location != None:
            dispatcher.utter_message(template='utter_detected_location', afspraak_location=detected_location)
        return [SlotSet('stadswinkel_location', detected_location)]

