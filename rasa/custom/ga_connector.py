from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import inspect

import logging

from sanic import Blueprint, response
from sanic.request import Request

from rasa.core.channels.channel import UserMessage, OutputChannel
from rasa.core.channels.channel import InputChannel
from rasa.core.channels.channel import CollectingOutputChannel
from typing import Any, Dict, List, Text
import re

import json	

logger = logging.getLogger(__name__)

class GoogleAssistantOutput(CollectingOutputChannel):
    """Output channel that collects send messages in a list"""

    @classmethod
    def name(cls) -> Text:
        return "google_home"

    # # TODO: Implement all of the following functions to match the Google Speech APIs JSON format. See README.

    # def __init__(self):
    #     self.messages = []

    # @classmethod
    # def name(cls):
    #     return 'google_home'

    # @staticmethod
    # def _message(recipient_id, text=None, image=None, buttons=None, attachment=None, custom=None):
    #     obj = {
    #         'recipient_id': recipient_id,
    #         'text': text,
    #         'image': image,
    #         'buttons': buttons,
    #         'attachment': attachment,
    #         'custom': custom,
    #     }
    #     logger.info("object: {0}".format(str(obj)))
    #     value = {k: v for k, v in obj.items() if v is not None}
    #     logger.info("value: {0}".format(str(value)))
    #     return value

    # def latest_output(self):
    #     if self.messages:
    #         return self.messages[-1]
    #     else:
    #         return None

    # async def send_text_message(self, recipient_id: Text, text: Text, **kwargs: Any) -> None:
    #     logger.info("user message: {0}".format(text))
    #     for message_part in text.split('\n\n'):
    #         await self.messages.append(self._message(recipient_id, text=message_part))

    # async def send_image_url(self, recipient_id: Text, image: Text, **kwargs: Any) -> None:
    #     await self.messages.append(self._message(recipient_id, image=image))

    # async def send_attachment(self, recipient_id: Text, attachment: Text, **kwargs: Any) -> None:
    #     await self.messages.append(self._message(recipient_id, attachment=attachment))

    # async def send_text_with_buttons(
    #         self,
    #         recipient_id: Text,
    #         text: Text,
    #         buttons: List[Dict[Text, Any]],
    #         **kwargs: Any
    # ) -> None:
    #     logger.info("list: {0}".format(str(self.messages)))
    #     msg = self._message(recipient_id, text=text, buttons=buttons)
    #     logger.info("message: {0}".format(str(msg)))
    #     await self.messages.append(msg)

    # async def send_custom_json(self, recipient_id: Text, json_message: Dict[Text, Any], **kwargs: Any) -> None:
    #     await self.messages.append(self._message(recipient_id, custom=json_message))



class GoogleConnector(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    @classmethod
    def name(cls):
        return "google_home"


    def blueprint(self, on_new_message):
    
        google_webhook = Blueprint("google_webhook", __name__)


        @google_webhook.route("/", methods=['GET'])
        async def health(request: Request):
            return response.json({"status": "ok"})

        @google_webhook.route("/webhook", methods=['POST'])
        async def receive(request: Request):
            
            payload = request.json		
            sender_id = payload['conversation']['conversationId']
            intent = payload['inputs'][0]['intent'] 			
            text = payload['inputs'][0]['rawInputs'][0]['query'] 		

            response_json = {
                  "conversationToken": "{\"state\":null,\"data\":{}}",
                  "expectUserResponse": 'true',
                  "expectedInputs": [
                    {
                      "inputPrompt": {
                        "richInitialPrompt": {
                          "items": [],
                          "suggestions": [],
                          "linkOutSuggestion": {}
                        }
                      },
                      "possibleIntents": [
                        {
                          "intent": "actions.intent.TEXT"
                        }
                      ]
                    }
                  ]
               }
               
            items = response_json["expectedInputs"][0]["inputPrompt"]["richInitialPrompt"]["items"]
            suggestions = response_json["expectedInputs"][0]["inputPrompt"]["richInitialPrompt"]["suggestions"]
            linkOutSuggestion = response_json["expectedInputs"][0]["inputPrompt"]["richInitialPrompt"]["linkOutSuggestion"]
            
            #TODO: Clean code and create seperate functions for all ssml rules
            #TODO: Remove logger.info's 
            
            if intent == 'actions.intent.MAIN':	
                firstResponse = {"simpleResponse": {"ssml":"<speak><emphasis level=\"reduced\">Hallo,<\/emphasis><break time=\"300ms\"\/><emphasis level=\"reduced\">mijn naam is Gem! <\/emphasis><break time=\"200ms\"\/><emphasis level=\"reduced\">Ik wil je graag helpen. <\/emphasis><break time=\"200ms\"\/><emphasis level=\"reduced\">Wat kan ik voor je doen?<\/emphasis><\/speak>"}} 
                items.append(firstResponse)
            else:
                out = GoogleAssistantOutput()
                logger.debug(out)
                customData = {"municipality":"Tilburg"}
                await on_new_message(
                  UserMessage(text, out, sender_id, input_channel=GoogleConnector.name(), metadata=customData)
                  )
                logger.info('out.messages')
                logger.info(out.messages)
                responses = [m.get('text') for m in out.messages if m.get('text') is not None]

                try:
                    logger.info(responses)
                    logger.info(re.findall("\[(.*)\]", responses[0]))
                    all_urls = re.findall("(?:__|[*#])|\[(.*?)\]\(.*?\)", responses[0])

                    for title in all_urls:            
                        sitetitle = re.compile( "\[(.*)\]" ).search(responses[0]).group(1)
                        logger.info('sitetitle1')
                        logger.info(sitetitle)
                        sitetitle2 = re.sub(r'\([^)]*\)', '', sitetitle)
                        logger.info('sitetitle2')
                        logger.info(sitetitle2)
                        url = re.compile( "\]\((.*)\)" ).search(responses[0]).group(1)
                        responses[0] = responses[0].replace("[{0}]".format(sitetitle), sitetitle2)                 
                        responses[0] = responses[0].replace("({0})".format(url), "")

                        linkOut = {"destinationName": sitetitle, "url": url}
                        linkOutSuggestion.update(linkOut)


                except Exception as e:
                  logger.info('in eerste exeption')
                  logger.debug(e)
                  pass

                # if len(responses) > 1:
                logger.info('responses')
                logger.info(responses)

                # Include <speak> tags and automatic SSML breaks
                if '<speak>' not in responses[0]:
                  responses[0] = "<speak><p>" + responses[0] + " </speak>"               
                responses[0] = responses[0].replace('. ', '.</p><p>')
                responses[0] = responses[0].replace('? ', '?</p><p>')
                responses[0] = responses[0].replace('! ', '!</p><p>')
                responses[0] = responses[0].replace('<p></speak>', '</speak>')
                responses[0] = responses[0].replace(' </speak>', '</p></speak>')
                responses[0] = responses[0].replace(', <break strength="medium"/>', ',<break strength="medium"/>')
                responses[0] = responses[0].replace(', ', ',<break strength="medium"/>')
                logger.info('Response 0')
                logger.info(responses[0])

                #TODO: create a function rule to strip certain formats
                message0 = {"simpleResponse": {"textToSpeech":responses[0].replace("*","")}}
                items.append(message0)

                # else:
                #   message0 = {"simpleResponse": {"textToSpeech":responses[0] + " Kan ik je verder nog ergens mee helpen?"}}
                #   items.append(message0)

                try:
                  buttons = [m["buttons"][0]["title"] for m in out.messages]
                except:
                  buttons = []
                  
                if len(responses) > 1:
                  if len(responses) > 2:
                    try:
                      logger.info(re.findall("\[(.*)\]", responses[2]))
                      all_urls = re.findall("(?:__|[*#])|\[(.*?)\]\(.*?\)", responses[2])
                      logger.info('IN DE RESPONSE')    
                      logger.info(responses)
                      logger.info(responses[2])  

                      for title in all_urls:      
                        sitetitle = re.compile( "\[(.*)\]" ).search(responses[2]).group(1)
                        logger.info('sitetitle1')
                        logger.info(sitetitle)
                        sitetitle2 = re.sub(r'\([^)]*\)', '', sitetitle)
                        logger.info('sitetitle2')
                        logger.info(sitetitle2)
                        url = re.compile( "\]\((.*)\)" ).search(responses[2]).group(1)
                        responses[2] = responses[2].replace("[{0}]".format(sitetitle), sitetitle2)                 
                        responses[2] = responses[2].replace("({0})".format(url), "")

                        linkOut = {"destinationName": sitetitle, "url": url}
                        linkOutSuggestion.update(linkOut)
                    except Exception as e:
                      logger.debug(e)
                      pass
                    
                    try:
                      logger.info(re.findall("\[(.*)\]", responses[1]))
                      all_urls = re.findall("(?:__|[*#])|\[(.*?)\]\(.*?\)", responses[1])

                      for title in all_urls:            
                        sitetitle = re.compile( "\[(.*)\]" ).search(responses[1]).group(1)
                        logger.info('sitetitle1')
                        logger.info(sitetitle)
                        sitetitle2 = re.sub(r'\([^)]*\)', '', sitetitle)
                        logger.info('sitetitle2')
                        logger.info(sitetitle2)
                        url = re.compile( "\]\((.*)\)" ).search(responses[1]).group(1)
                        responses[1] = responses[1].replace("[{0}]".format(sitetitle), sitetitle2)                 
                        responses[1] = responses[1].replace("({0})".format(url), "")

                        linkOut = {"destinationName": sitetitle, "url": url}
                        linkOutSuggestion.update(linkOut)

                    except Exception as e:
                      logger.debug(e)
                      pass

                    # Include <speak> tags and automatic SSML breaks
                    if '</speak>' in responses[1]:
                      responses[1] = responses[1].replace('</speak>', '')                                  
                    if '<speak>' not in responses[1]:
                      responses[1] = "<speak><p>" + responses[1] + "</p>"              
                    responses[1] = responses[1].replace('. ', '.</p><p>')
                    responses[1] = responses[1].replace('? ', '?</p><p>')
                    responses[1] = responses[1].replace('! ', '!</p><p>')
                    responses[1] = responses[1].replace(', <break strength="medium"/>', ',<break strength="medium"/>')
                    responses[1] = responses[1].replace(', ', ',<break strength="medium"/>')
                    logger.info('Response 1')
                    logger.info(responses[1])
                    if '<speak>' in responses[2]:
                      responses[2] = responses[2].replace('<speak>', '')                                  
                    if '</speak>' not in responses[2]:
                      responses[2] = "<p>" + responses[2] + " </speak>"
                    responses[2] = responses[2].replace('. ', '.</p><p>')
                    responses[2] = responses[2].replace('? ', '?</p><p>')
                    responses[2] = responses[2].replace('! ', '!</p><p>')
                    responses[2] = responses[2].replace('<p></speak>', '</speak>')
                    responses[2] = responses[2].replace(' </speak>', '</p></speak>')
                    responses[2] = responses[2].replace(', <break strength="medium"/>', ',<break strength="medium"/>')
                    responses[2] = responses[2].replace(', ', ',<break strength="medium"/>')
                    logger.info('Response 2')
                    logger.info(responses[2])

                    #TODO: create a function rule to strip certain formats
                    message2 = {"simpleResponse": {"textToSpeech":responses[1].replace("*","") + " " + responses[2].replace("*","")}}
                    items.append(message2)
                  
                  else:
                    try:
                      logger.info(re.findall("\[(.*)\]", responses[1]))
                      all_urls = re.findall("(?:__|[*#])|\[(.*?)\]\(.*?\)", responses[1])

                      for title in all_urls:            
                        sitetitle = re.compile( "\[(.*)\]" ).search(responses[1]).group(1)
                        logger.info('sitetitle1')
                        logger.info(sitetitle)
                        sitetitle2 = re.sub(r'\([^)]*\)', '', sitetitle)
                        logger.info('sitetitle2')
                        logger.info(sitetitle2)
                        url = re.compile( "\]\((.*)\)" ).search(responses[1]).group(1)
                        responses[1] = responses[1].replace("[{0}]".format(sitetitle), sitetitle2)                 
                        responses[1] = responses[1].replace("({0})".format(url), "")

                        linkOut = {"destinationName": sitetitle, "url": url}
                        linkOutSuggestion.update(linkOut)
                    except Exception as e:
                      logger.debug(e)
                      pass
                      
                    if '<speak>' not in responses[1]:
                      responses[1] = "<speak> <p>" + responses[1] + " </speak>"               
                    responses[1] = responses[1].replace('. ', '.</p><p>')
                    responses[1] = responses[1].replace('? ', '?</p><p>')
                    responses[1] = responses[1].replace('! ', '!</p><p>')
                    responses[1] = responses[1].replace('<p></speak>', '</speak>')
                    responses[1] = responses[1].replace(' </speak>', '</p></speak>')
                    responses[1] = responses[1].replace(', <break strength="medium"/>', ',<break strength="medium"/>')
                    responses[1] = responses[1].replace(', ', ',<break strength="medium"/>')
                    logger.info('Response 1')
                    logger.info(responses[1])

                    #TODO: create a function rule to strip certain formats
                    message1 = {"simpleResponse": {"textToSpeech":responses[1].replace("*","")}}
                    items.append(message1)


                if len(buttons) > 0:
                  buttons_str = ""
                  for title in out.messages[0]["buttons"]:
                    if 'web_url' in title.values():
                        logger.debug('TITEL PAYLOAD')
                        logger.debug(title["payload"])
                        linkOut = {"destinationName": title["title"], "url": title["payload"]}
                        linkOutSuggestion.update(linkOut)
                    else:
                      button = title["title"]
                      buttons_str += button + " of "
                      buttonmessage = {"title":button}
                      suggestions.append(buttonmessage)

                  
                  # buttons_str = buttons_str[:-4]
                  # buttonmessage = {"simpleResponse": {"textToSpeech":"Misschien bedoel je: " + buttons_str}}
                  # items.append(buttonmessage)

                logger.debug(buttons)
                logger.debug(out.messages)	
                logger.debug(response_json)	

            return response.json(response_json)		
          		
        return google_webhook