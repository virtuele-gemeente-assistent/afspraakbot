import asyncio
import logging
from typing import Text, Dict, Optional, Callable, Awaitable, Any, List

from sanic import Blueprint, response
from sanic.request import Request

from rasa.core.channels.channel import (
    CollectingOutputChannel,
    UserMessage,
    InputChannel,
)

from rasa.core.channels.rest import RestInput

from rasa.shared.core.events import ConversationResumed, Restarted, ActionExecuted, FollowupAction
from rasa.utils.endpoints import EndpointConfig, ClientResponseError
from sanic.response import HTTPResponse

logger = logging.getLogger(__name__)


class CallbackOutput(CollectingOutputChannel):
    @classmethod
    def name(cls) -> Text:
        return "callback"

    def __init__(self, endpoint: EndpointConfig) -> None:

        self.callback_endpoint = endpoint
        super().__init__()

    @staticmethod
    def _message(
        recipient_id: Text,
        text: Text = None,
        image: Text = None,
        buttons: List[Dict[Text, Any]] = None,
        attachment: Text = None,
        custom: Dict[Text, Any] = None,
    ) -> Dict:
        obj = CollectingOutputChannel._message(
            recipient_id,
            text=text,
            image=image,
            buttons=buttons,
            attachment=attachment,
            custom=custom,
        )

        # Use quick replies instead of buttons,
        # see https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/3002
        if obj.get("buttons", None):
            obj["quick_replies"] = obj.pop("buttons")

        return obj

    async def _persist_message(self, message: Dict[Text, Any]) -> None:
        await super()._persist_message(message)

        try:
            await self.callback_endpoint.request(
                "post", content_type="application/json", json=message
            )
        except ClientResponseError as e:
            logger.error(
                "Failed to send output message to callback. "
                "Status: {} Response: {}"
                "".format(e.status, e.text)
            )


class CallbackInput(RestInput):
    """A custom REST http input channel that responds using a callback server.

    Incoming messages are received through a REST interface. Responses
    are sent asynchronously by calling a configured external REST endpoint."""

    @classmethod
    def name(cls) -> Text:
        return "callback"

    @classmethod
    def from_credentials(cls, credentials: Optional[Dict[Text, Any]]) -> InputChannel:
        return cls(EndpointConfig.from_dict(credentials))

    def __init__(self, endpoint: EndpointConfig) -> None:
        self.callback_endpoint = endpoint

    def get_metadata(self, request: Request) -> Optional[Dict[Text, Any]]:
        return {**request.json.get("customData", {}), **request.json.get("metadata", {})}

    def _extract_input_channel(self, req: Request) -> Text:
        return req.json.get("input_channel") or self.name()

    def blueprint(
        self, on_new_message: Callable[[UserMessage], Awaitable[Any]]
    ) -> Blueprint:
        callback_webhook = Blueprint("callback_webhook", __name__)

        @callback_webhook.route("/webhook", methods=["GET"])
        async def health(_: Request):
            return response.json({"status": "ok"})

        @callback_webhook.route("/webhook", methods=["POST"])
        async def webhook(request: Request) -> HTTPResponse:
            sender_id = await self._extract_sender(request)
            text = self._extract_message(request)
            metadata = self.get_metadata(request)
            input_channel = self._extract_input_channel(request)

            collector = self.get_output_channel()

            if request.json.get("custom", {}).get("deescalate", False):
                # Resume the conversation
                tracker = request.app.agent.tracker_store
                tracker_state = tracker.retrieve(sender_id)
                tracker_state.update(ConversationResumed())
                tracker.save(tracker_state)

                # FIXME directly trigger utter instead of injecting user message
                asyncio.ensure_future(on_new_message(
                    UserMessage("/ask_for_chatbot", collector, sender_id, metadata=metadata, input_channel=input_channel)
                ))

                return response.text("success")

            if text:
                asyncio.ensure_future(on_new_message(
                    UserMessage(text, collector, sender_id, metadata=metadata, input_channel=input_channel)
                ))
            return response.text("success")

        return callback_webhook

    def get_output_channel(self) -> CollectingOutputChannel:
        return CallbackOutput(self.callback_endpoint)